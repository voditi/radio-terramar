$(document).ready(function () {
  $('.tab-item a').click(function () {
    var a = $(this);
    var active_tab_class = 'active';
    var the_tab = '.' + a.attr('data-tab');

    $('.tab-item a').removeClass(active_tab_class);
    a.addClass(active_tab_class);

    $('.tabs-content .tabs').css({
      'display': 'none'
    });

    $(the_tab).show();

    return false;
  });
});
$(document).ready(function () {
  $(".btn-home").click(function () {
    $("#main-wrapper").load("home.html");
  });

  $(".btn-historia").click(function () {
    $("#main-wrapper").load("historia.html");
  });

  $(".btn-equipe").click(function () {
    $("#main-wrapper").load("equipe.html");
  });

  $(".btn-programacao").click(function () {
    $("#main-wrapper").load("programacao.html");
  });

  $(".btn-promocoes").click(function () {
    $("#main-wrapper").load("promocoes.html");
  });

  $(".btn-fotos").click(function () {
    $("#main-wrapper").load("fotos.html");
  });

  $(".btn-videos").click(function () {
    $("#main-wrapper").load("videos.html");
  });

  $(".btn-noticias").click(function () {
    $("#main-wrapper").load("noticias.html");
  });

  $(".btn-contato").click(function () {
    $("#main-wrapper").load("contato.html");
  });

  $(".btn-noticia").click(function () {
    $("#main-wrapper").load("noticia.html");
  });
});